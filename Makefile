.PHONY: release start deploy requirements freeze syncdb run user cloc clean

ARCHIVE:="django_deploy.tar"
CURRENT_DIR:=$(CURDIR)

project_name=printwall

release: start deploy

start:
	ansible-playbook -i ansible-django-deploy/hosts ansible-django-deploy/root-playbook.yml

deploy:
	tar -cf /tmp/$(ARCHIVE) *
	ansible-playbook -i ansible-django-deploy/hosts ansible-django-deploy/user-playbook.yml
	rm -rf /tmp/$(ARCHIVE)

requirements:
	-@echo "### Installing requirements"
	-@pip install -r requirements.txt

freeze:
	-@echo "### Freezing python packages to requirements.txt"
	-@pip freeze > requirements.txt

syncdb:
	-@echo "### Creating database tables and loading fixtures"
	@PYTHONPATH=$(PYTHONPATH):. DJANGO_SETTINGS_MODULE=$(project_name).settings python manage.py makemigrations
	@PYTHONPATH=$(PYTHONPATH):. DJANGO_SETTINGS_MODULE=$(project_name).settings python manage.py migrate

run:
	@PYTHONPATH=$(PYTHONPATH):. DJANGO_SETTINGS_MODULE=$(project_name).settings python manage.py runserver --insecure

user:
	-@python manage.py createsuperuser

cloc:
	-@echo "### Counting lines of code within the project"
	-@echo "# Total:" ; find . -iregex '.*\.py\|.*\.js\|.*\.html\|.*\.css' -type f -exec cat {} + | wc -l
	-@echo "# Python:" ; find . -name '*.py' -type f -exec cat {} + | wc -l
	-@echo "# JavaScript:" ; find . -name '*.js' -type f -exec cat {} + | wc -l
	-@echo "# HTML:" ; find . -name '*.html' -type f -exec cat {} + | wc -l
	-@echo "# CSS:" ; find . -name '*.css' -type f -exec cat {} + | wc -l

clean:
	-@echo "### Cleaning *.pyc and .DS_Store files "
	-@find . -name '*.pyc' -exec rm -f {} \;
	-@find . -name '.DS_Store' -exec rm -f {} \;
