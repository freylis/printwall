from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^tinymce/', include('tinymce.urls')),

    url(r"^city/", include("apps.city.urls")),
    url(r"^catalog/", include("apps.catalog.urls")),
    url(r'', include("apps.article.urls")),
    url(r'^card/', include('apps.card.urls')),
    url(r'', include('apps.core.urls')),

    url(r'^$', 'printwall.views.home', name='home'),

)

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT}))
