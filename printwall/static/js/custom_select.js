/* JQuery Custom select plugin special for Baklazhan */
(function($){
	jQuery.fn.customSelect = function(options){
		options = $.extend({
			openAction: 'click',
			wrapClass: 'b-customselect',
			closeAction: false,
		}, options);
		// Opening
		$(document).on(options['openAction'], '.'+options['wrapClass'], function(){
			$(this).find('.'+options['wrapClass']+'__group').show().css('top', $(this).find('.'+options['wrapClass']+'__main').height()+2);
		})
		// Selecting
		.on('click', '.'+options['wrapClass']+'__option', function(){
			var t = $(this),
				select = t.parent().parent().prev();
			t.siblings().removeClass('selected');
			t.addClass('selected');
			t.parent().hide().parent().data('value', t.data('value')).children('b').html(t.html());
			select.find('option[value="'+t.data('value')+'"]').prop('selected', true);
			select.trigger('change');
			return false;
		})
		// Closing on document click
		.on('click', function(e){
			if($(e.target).parents('.'+options['wrapClass']).length){
				e.preventDefault();
				return;
			}
			$(this).find('.'+options['wrapClass']+'__group').hide();
		});
		// Closing on action
		if (options['closeAction'])
		{
			$(document).on(options['closeAction'], '.'+options['wrapClass'], function(){
				$(this).find('.'+options['wrapClass']+'__group').hide();
			})
		}
		return this.each(function(){
			var select = $(this),
				lnk = $('<span class="'+options['wrapClass']+'__main" data-value="'+select.val()+'"><b class="'+options['wrapClass']+'__text">'+(select.find('option:selected').data('html') || select.find('option:selected').text())+'</b><div class="'+options['wrapClass']+'__group"></div></span>'),
				groups = lnk.children('.'+options['wrapClass']+'__group');
			select.find('option').each(function(){
				var t = $(this),
					html = t.data('html') || t.text();
				groups.append('<span class="'+options['wrapClass']+'__option" data-value="'+t.val()+'">'+html+'</span>');
			});
			select.wrap('<span class="'+options['wrapClass']+'"></span>').hide();
			lnk.appendTo(select.parent());
		});
	}
})(jQuery);




$(function(){
	$('.custom_select').customSelect();
	$('.custom_pselect').customSelect({wrapClass: 'b-pselect'});
	$('.custom_pselect_poster').customSelect({wrapClass: 'b-pselectposter'});
});