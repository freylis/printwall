$(function(){
	// Выбор города
	$('#select_city_link').on('click', function(){
		$.fancybox($('#city_select_block').html(), {
			padding: 0,
			wrapCSS: 'fancybox_cityselect',
		});
		return false;
	});

	$('.fancybox').fancybox();
	$('[rel="fancybox"]').fancybox();

	//// Галерея
	$(document).on('click', '.b-good_mainpage .b-good__image', function(){
        $.fancybox($('#gallery_example').html(), {
			padding: 0,
             href: $(this).attr('link'),
             type: 'ajax'
		});
		return false;
	});

	$(document).on('click', '.b-gallery__min__item', function(){
		var t = $(this),
			gallery = t.parentsUntil('.b-gallery').parent().get(0);
		var img = $(gallery).find('.b-gallery__image > img');
		$('.b-gallery__min__item.active').removeClass('active');
		t.addClass('active');
		img.attr('src', t.data('image'));
		return false;
	});

	function switchPhoto(element, side)
	{
		if (side != 'next')
			side = 'prev';
		var other = side=='next' ? 'first-child' : 'last-child',
			gallery = $(element.parentsUntil('.b-gallery').parent().get(0)),
			min = gallery.find('.b-gallery__min__item.active'),
			img = gallery.find('.b-gallery__image > img'),
			next;
		if (min[side]().length)
			next = min[side]();
		else
			next = gallery.find('.b-gallery__min__item:'+other);			

		min.removeClass('active');
		next.addClass('active');
		img.attr('src', next.data('image'));
		return false;

	}

	$(document).on('click', '.b-gallery__arrow_left', function(){
		return switchPhoto($(this), 'prev');
	});
	$(document).on('click', '.b-gallery__arrow_right', function(){
		return switchPhoto($(this), 'next');
	});


	//// Жуткий хак, чтобы между пунктами меню поставить расстояние
	(function(){
		var length = 0,
			menu_width = $('.b-menu').width() - 75,
			menu_count = $('.b-menu__item').length;
		$('.b-menu_dynamic .b-menu__item').each(function(){
			length += $(this).width();
		});
		$('.b-menu_dynamic .b-menu__item').css('margin-left', Math.floor((menu_width - length) / menu_count))
	})();


	//// Меню в каталоге
	$('.b-catalog__menu__link').on('click', function(){
		var t = $(this);
		// Если нет подменю, то просто перейдём по ссылке
		if (!t.next().length)
			return true;
		if (t.next().is(':animated'))
			return false;
		// А если есть, то откроем или закроем его
		t.next().slideToggle(500);
		t.parent().toggleClass('active');
		return false;
	});

    //// Выбор типа фрезки
    $('.b-p13n2__block__link').on('click', function(){
        var link = $(this).attr('data');
        $('.b-p13n2__block__choose__tick').removeClass('active');
        $(this).children('.b-p13n2__block__choose__tick').addClass('active');
        $('.b-p13n__right__price__button').attr('href', link);
        return false;
    });

    //// Перснализация
    $('#b-p13n__example__colorselect__ok').on('click', function(){
       $('.b-p13n__example').css('background-color', '#' + $('#b-p13n__example__colorselect__hex').val());
        return false;
    });

});