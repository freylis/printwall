$(function(){
		//// Переключение вариантов в персонализации
	$('a.b-p13n__right__frame').on('click', function(){
		$(this).parent().find('.active').removeClass('active');
		$(this).addClass('active');
		return false;
	});

	$('.b-p13n__right__tonal__item').on('click', function(){
		$('.b-p13n__right__tonal__item.active').removeClass('active');
		$(this).addClass('active');
		return false;
	});

	$('a.b-p13n__right__fact').on('click', function(){
		$('.b-p13n__right__fact').removeClass('active');
		$(this).addClass('active');
		return false;
	});


	//// Слайдер в персонализации
	$('.b-slider__left, .b-slider__right').on('click', function(){
		var t = $(this);
		if (t.is('.b-slider__left'))
		{
			$('.i-slider__list > .b-good:last-child').prependTo('.i-slider__list');
			$('.i-slider__list').css('left', '-224px').animate({'left': '0'});
		}
		else
		{
			$('.i-slider__list').animate({'left': '-=224px'}, function(){
				$('.i-slider__list > .b-good:first-child').appendTo('.i-slider__list');
				$(this).css('left', '0');
			});
		}

		return false;
			
	});

	
	//// Выбор цвета в персонализации
	if ($('#p13n_color_select').length)
	{
		$('#p13n_color_select').on('click', function(){
			$('.b-p13n__example__colorselect').toggle();
			return false;
		});
		$('#b-p13n__example__colorselect__hex').val($('#p13n_color_select_color').data('hex'));
		$('#b-p13n__example__colorselect').ColorPicker({
			flat: true,
			color: $('#p13n_color_select_color').data('hex'),
			onChange: function (hsb, hex, rgb) {
				$('#b-p13n__example__colorselect__hex').val(hex);
			}
		});
		$('#b-p13n__example__colorselect__ok').on('click', function(){
			$('#p13n_color_select_color').data('hex', $('#b-p13n__example__colorselect__hex').val()).css('background-color', '#'+$('#b-p13n__example__colorselect__hex').val());
			$('.b-p13n__example__colorselect').hide();
			return false;
		});
		$('#b-p13n__example__colorselect__cancel').on('click', function(){
			$('#b-p13n__example__colorselect__hex').val($('#p13n_color_select_color').data('hex'));
			$('.b-p13n__example__colorselect').hide();
			return false;
		});
	}


	//// Подсветка залоченных инпутов
	$('.b-p13n__right__chooseown__input_locked').on({
		'focus': function(){
			$(this).parent().children('.b-p13n__right__chooseown__input_locked').addClass('b-p13n__right__chooseown__input_highlight');
		},
		'blur': function(){
			$(this).parent().children('.b-p13n__right__chooseown__input_locked').removeClass('b-p13n__right__chooseown__input_highlight');
		}
	})
});