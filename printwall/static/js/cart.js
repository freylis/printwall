$(function(){
	$('.b-cart__right__checkbox > input:checked').each(function(){
		$(this).parent().addClass('b-cart__right__checkbox_checked');
	});
	$('.b-cart__right__checkbox').on('click', function(){
		var t = $(this),
			i = t.children('input');
		if (i.is(':checked'))
		{
			i.removeProp('checked');
			t.removeClass('b-cart__right__checkbox_checked');
		}
		else
		{
			i.prop('checked', 'checked');
			t.addClass('b-cart__right__checkbox_checked');
		}
		return false;
	});

	$('.b-cart__right__quest').on('click', function(){
		var hint_block = $('#cart_hint_block'),
			t = $(this),
			offset = t.offset(),
			win = $(window),
			right = win.width() - offset.left - t.outerWidth(),
			bottom = win.height() - offset.top - t.outerHeight();
		if (!hint_block.length)
		{
			hint_block = $('<div class="b-cart__hint" id="cart_hint_block"></div>');
			hint_block.appendTo('body');
		}
		hint_block.html(t.next('.b-cart__right__hint').html()).css({bottom: bottom + 20, right: right}).show();
		return false;
	});

	$(document).on('click', function(){
		$('#cart_hint_block').hide();
	}).on('click', '#cart_hint_block', function(e){
		e.preventDefault();
		e.stopPropagation();
		return false;
	});

	$('.b-cart__order__head__tab').on('click', function(){
		$('.b-cart__order__head__tab.active').removeClass('active');
		$(this).addClass('active');
		$('.cart_tab_content').hide();

		$('.cart_tab_content[data-id="'+$(this).data('id')+'"]').show();
		return false;
	});

	$('.b-cart__order__payment__radio input:checked').parent().addClass('active');

	$('.b-cart__order__payment').on('click', function(){
		console.log($(this).children('.b-cart__order__payment__radio').data('name'));
		var name = $(this).children('.b-cart__order__payment__radio').data('name');
		$('.b-cart__order__payment__radio[data-name="'+name+'"] input').removeProp('checked');
		$('.b-cart__order__payment__radio[data-name="'+name+'"]').removeClass('active');
		$(this).children('.b-cart__order__payment__radio').addClass('active').children('input').prop('checked', 'checked');
		return false;
	});

	$('.b-cart__order__terms__checkbox > input:checked').parent().addClass('active');

	$('.b-cart__order__terms__checkbox').on('click', function(){
		var t = $(this),
			i = t.children('input');

		if (i.is(':checked'))
		{
			i.removeProp('checked');
			t.removeClass('active');
		}
		else
		{
			i.prop('checked', 'checked');
			t.addClass('active');
		}
		return false;
	});
});