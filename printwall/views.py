# coding: utf-8

from django.shortcuts import render

from apps.catalog.models import ItemType


def home(request):

    content_types = ItemType.objects.filter(enable=True)

    items = []
    for ctype in content_types:
        model = ctype.ctype.model_class()
        items += list(model.objects.filter(at_main=True))

    return render(request, "index.html", {
        "goods": items,
    })