# coding: utf-8

from django.db import models


class BaseMoveModel(models.Model):

    class Meta:
        abstract = True
        ordering = ("-order",)

    order = models.IntegerField(null=True, blank=False, default=1, editable=False)

    def move_up_link(self):
        return "<a href=''>u</a>"

    def move_down_link(self):
        return "<a href=''>d</a>"

