# coding: utf-8

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '0&!)te3%ednc+t%^t+m!&!iokx46!(=d6b=$4$y_#zu2cdl+^*'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    'django.core.context_processors.request',
    "apps.city.context_processors.current_city",
    'apps.options.context_processors.texture_type',
    'apps.project.context_processors.project_constants',
)

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'sorl.thumbnail',
    'tinymce',
    'django_geoip',
    'mptt',
    'pure_pagination',
    'debug_toolbar',

    "apps",
    "apps.article",
    "apps.catalog",
    "apps.flatblock",
    "apps.city",
    "apps.banner",
    'apps.options',
    'apps.project',
    'apps.core',
)

MIDDLEWARE_CLASSES = (
    'django_geoip.middleware.LocationMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'apps.city.middleware.CurrentCityMiddleware',
)

ROOT_URLCONF = 'printwall.urls'

WSGI_APPLICATION = 'printwall.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'ru-RU'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = 'static/'

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'printwall/media')
print MEDIA_ROOT

TEMPLATE_DIRS = (
    os.path.abspath(os.path.join(BASE_DIR, 'printwall', 'templates')),
)

STATICFILES_DIRS = (
    os.path.abspath(os.path.join(BASE_DIR, 'printwall', 'static')),
)

#sorl.thumbnail
THUMBNAIL_DEBUG = False

#pure_pagination
PAGINATION_SETTINGS = {
    'PAGE_RANGE_DISPLAYED': 10,
    'MARGIN_PAGES_DISPLAYED': 2,
}


try:
    from local_settings import *
except ImportError:
    pass
