from django.contrib import admin

from .models import Feedback, FeedbackPhoto, FaqSection, FaqContent, Reviews,\
                    ReviewsPhoto
from sorl.thumbnail.admin import AdminImageMixin


class FeedbackPhotoAdminInline(AdminImageMixin, admin.TabularInline):
    model = FeedbackPhoto

    def has_add_permission(self, request):
        return


class FeedbackAdmin(admin.ModelAdmin):
    list_display = ('email', 'text', 'created')
    list_filter = ('created',)
    date_hierarchy = 'created'
    inlines = (FeedbackPhotoAdminInline,)

    def has_add_permission(self, request):
        return


class FaqContentAdminInline(admin.TabularInline):
    model = FaqContent
    extra = 1


class FaqSectionAdmin(admin.ModelAdmin):
    list_display = ('name',)
    inlines = (FaqContentAdminInline,)


class ReviewsPhotoAdminInline(AdminImageMixin, admin.TabularInline):
    model = ReviewsPhoto

    def has_add_permission(self, request):
        return


class ReviewsAdmin(admin.ModelAdmin):
    list_display = ('name', 'publish', 'created')
    list_filter = ('publish', 'created')
    date_hierarchy = 'created'
    inlines = (ReviewsPhotoAdminInline,)

    def has_add_permission(self, request):
        return


admin.site.register(Feedback, FeedbackAdmin)
admin.site.register(FaqSection, FaqSectionAdmin)
admin.site.register(Reviews, ReviewsAdmin)
