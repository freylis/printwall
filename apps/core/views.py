# -*- coding: utf-8 -*-
from django.views.generic import ListView
from django.http import JsonResponse, Http404
from django.shortcuts import get_object_or_404

from apps.city.models import City
from .models import FeedbackPhoto, FaqSection, Reviews, ReviewsPhoto
from .forms import FeedbackForm, FeedbackPhotoForm, ReviewsForm,\
                   ReviewsPhotoForm
from pure_pagination.mixins import PaginationMixin
from apps.catalog.views import ON_PAGE_DEFAULT, ON_PAGE_VARIANTS


class ContactView(ListView):
    template_name = 'core/contact.html'
    context_object_name = 'cities'

    def get_queryset(self):
        return City.objects.exclude(pk=self.request.current_city.pk)

    def get_context_data(self, **kwargs):
        ctx = super(ContactView, self).get_context_data(**kwargs)
        ctx['form'] = FeedbackForm()
        return ctx


class FaqSectionView(ListView):
    model = FaqSection


class ReviewsView(PaginationMixin, ListView):
    paginate_by = ON_PAGE_DEFAULT

    def get_paginate_by(self, *args, **kwargs):
        paginate = int(self.request.GET.get('paginate', self.paginate_by))
        if paginate in ON_PAGE_VARIANTS:
            return paginate
        return self.paginate_by

    def get_queryset(self):
        return Reviews.objects.filter(publish=True)

    def get_context_data(self, **kwargs):
        ctx = super(ReviewsView, self).get_context_data(**kwargs)
        ctx['form'] = ReviewsForm()
        ctx['on_page_variants'] = ON_PAGE_VARIANTS
        ctx['on_page'] = self.get_paginate_by()
        return ctx


def feedback_img_upload(request):
    if request.is_ajax and request.method == 'POST':
        form = FeedbackPhotoForm(request.POST, request.FILES)
        if form.is_valid():
            p = form.save(commit=False)
            p.session_key = request.session.session_key
            p.save()
            data = {'success': True, 'img': p.image.path.split('/')[-1],
                    'delete_url': p.get_delete_url()}
        else:
            data = {}
            data['form_errors'] = form.errors
        return JsonResponse(data)

    raise Http404


def feedback_img_delete(request, pk):
    p = get_object_or_404(FeedbackPhoto, pk=pk,
                          session_key=request.session.session_key,
                          feedback__isnull=True)
    p.delete()
    data = {'success': True}
    return JsonResponse(data)


def feedback_save(request):
    if request.is_ajax and request.method == 'POST':
        form = FeedbackForm(request.POST)
        if form.is_valid():
            p = form.save()
            FeedbackPhoto.objects.filter(feedback__isnull=True,
                                         session_key=request.session.session_key)\
                                 .update(feedback=p)
            data = {'success': True}
        else:
            data = {}
            data['form_errors'] = form.errors
        return JsonResponse(data)

    raise Http404


def reviews_img_upload(request):
    if request.is_ajax and request.method == 'POST':
        form = ReviewsPhotoForm(request.POST, request.FILES)
        if form.is_valid():
            p = form.save(commit=False)
            p.session_key = request.session.session_key
            p.save()
            data = {'success': True, 'img': p.image.path.split('/')[-1],
                    'delete_url': p.get_delete_url()}
        else:
            data = {}
            data['form_errors'] = form.errors
        return JsonResponse(data)

    raise Http404


def reviews_img_delete(request, pk):
    p = get_object_or_404(ReviewsPhoto, pk=pk,
                          session_key=request.session.session_key,
                          reviews__isnull=True)
    p.delete()
    data = {'success': True}
    return JsonResponse(data)


def reviews_save(request):
    if request.is_ajax and request.method == 'POST':
        form = ReviewsForm(request.POST)
        if form.is_valid():
            p = form.save()
            ReviewsPhoto.objects.filter(reviews__isnull=True,
                                        session_key=request.session.session_key)\
                                .update(reviews=p)
            data = {'success': True}
        else:
            data = {}
            data['form_errors'] = form.errors
        return JsonResponse(data)

    raise Http404
