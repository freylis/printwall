# -*- coding: utf-8 -*-
#В проект попал на доработку, приложения для общих страниц не нашёл
#поэтому создал отдельное @scream4ik
from django.db import models
from django.core.urlresolvers import reverse

from printwall.utils import random_path
from tinymce.models import HTMLField
from sorl.thumbnail import ImageField


class Feedback(models.Model):
    email = models.EmailField(u'Ваш E-mail')
    text = models.TextField(u'Текст')
    created = models.DateTimeField(u'Дата', auto_now_add=True)

    class Meta:
        verbose_name = verbose_name_plural = u'Обратная связь'

    def __unicode__(self):
        return self.email


class FeedbackPhoto(models.Model):
    feedback = models.ForeignKey(Feedback, verbose_name=u'Обратная связь', blank=True, null=True)
    session_key = models.CharField(max_length=100, editable=False)
    image = ImageField(u'Изображение', upload_to=random_path, max_length=250, blank=True, null=True)

    class Meta:
        verbose_name = verbose_name_plural = u'Фото'

    def get_delete_url(self):
        return reverse('feedback_img_delete', args=[self.pk])

    def __unicode__(self):
        return str(self.pk)


class FaqSection(models.Model):
    name = models.CharField(u'Название', max_length=100)

    class Meta:
        verbose_name = u'Секцию'
        verbose_name_plural = u'Вопросы и ответы'

    def __unicode__(self):
        return self.name


class FaqContent(models.Model):
    section = models.ForeignKey(FaqSection, verbose_name=u'Секция', related_name='content')
    question = models.CharField(u'Вопрос', max_length=200)
    answer = HTMLField(u'Ответ')

    class Meta:
        verbose_name = verbose_name_plural = u'Вопрос / ответ'

    def __unicode__(self):
        return self.question


class Reviews(models.Model):
    name = models.CharField(u'Имя', max_length=50)
    city = models.CharField(u'Город', max_length=80, blank=True)
    review = models.TextField(u'Отзыв')
    publish = models.BooleanField(u'Публиковать', default=False)
    created = models.DateTimeField(u'Дата', auto_now_add=True)

    class Meta:
        verbose_name = verbose_name_plural = u'Отзывы'
        ordering = ('-created',)

    def __unicode__(self):
        return self.name


class ReviewsPhoto(models.Model):
    review = models.ForeignKey(Reviews, verbose_name=u'Отзыв', blank=True, null=True, related_name='photo')
    session_key = models.CharField(max_length=100, editable=False)
    image = ImageField(u'Изображение', upload_to=random_path, max_length=250, blank=True, null=True)

    class Meta:
        verbose_name = verbose_name_plural = u'Изображение'

    def get_delete_url(self):
        return reverse('reviews_img_delete', args=[self.pk])

    def __unicode__(self):
        return str(self.pk)
