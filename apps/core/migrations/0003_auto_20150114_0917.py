# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_feedback_created'),
    ]

    operations = [
        migrations.CreateModel(
            name='FaqContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question', models.CharField(max_length=200, verbose_name='\u0412\u043e\u043f\u0440\u043e\u0441')),
                ('answer', tinymce.models.HTMLField(verbose_name='\u041e\u0442\u0432\u0435\u0442')),
            ],
            options={
                'verbose_name': '\u0412\u043e\u043f\u0440\u043e\u0441 / \u043e\u0442\u0432\u0435\u0442',
                'verbose_name_plural': '\u0412\u043e\u043f\u0440\u043e\u0441 / \u043e\u0442\u0432\u0435\u0442',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FaqSection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u0421\u0435\u043a\u0446\u0438\u044e',
                'verbose_name_plural': '\u0412\u043e\u043f\u0440\u043e\u0441\u044b \u0438 \u043e\u0442\u0432\u0435\u0442\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='faqcontent',
            name='section',
            field=models.ForeignKey(verbose_name='\u0421\u0435\u043a\u0446\u0438\u044f', to='core.FaqSection'),
            preserve_default=True,
        ),
    ]
