# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedback',
            name='created',
            field=models.DateTimeField(default='2015-12-12', verbose_name='\u0414\u0430\u0442\u0430', auto_now_add=True),
            preserve_default=False,
        ),
    ]
