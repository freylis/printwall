# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields
import printwall.utils


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_auto_20150114_0931'),
    ]

    operations = [
        migrations.AlterField(
            model_name='feedbackphoto',
            name='image',
            field=sorl.thumbnail.fields.ImageField(max_length=250, upload_to=printwall.utils.random_path, null=True, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='reviewsphoto',
            name='image',
            field=sorl.thumbnail.fields.ImageField(max_length=250, upload_to=printwall.utils.random_path, null=True, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='reviewsphoto',
            name='review',
            field=models.ForeignKey(related_name='photo', verbose_name='\u041e\u0442\u0437\u044b\u0432', blank=True, to='core.Reviews', null=True),
            preserve_default=True,
        ),
    ]
