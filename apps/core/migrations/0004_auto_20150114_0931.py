# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import printwall.utils


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20150114_0917'),
    ]

    operations = [
        migrations.CreateModel(
            name='Reviews',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name='\u0418\u043c\u044f')),
                ('city', models.CharField(max_length=80, verbose_name='\u0413\u043e\u0440\u043e\u0434', blank=True)),
                ('review', models.TextField(verbose_name='\u041e\u0442\u0437\u044b\u0432')),
                ('publish', models.BooleanField(default=False, verbose_name='\u041f\u0443\u0431\u043b\u0438\u043a\u043e\u0432\u0430\u0442\u044c')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430')),
            ],
            options={
                'ordering': ('-created',),
                'verbose_name': '\u041e\u0442\u0437\u044b\u0432\u044b',
                'verbose_name_plural': '\u041e\u0442\u0437\u044b\u0432\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ReviewsPhoto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('session_key', models.CharField(max_length=100, editable=False)),
                ('image', models.ImageField(max_length=250, upload_to=printwall.utils.random_path, null=True, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', blank=True)),
                ('review', models.ForeignKey(verbose_name='\u041e\u0442\u0437\u044b\u0432', blank=True, to='core.Reviews', null=True)),
            ],
            options={
                'verbose_name': '\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435',
                'verbose_name_plural': '\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435',
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='faqcontent',
            name='section',
            field=models.ForeignKey(related_name='content', verbose_name='\u0421\u0435\u043a\u0446\u0438\u044f', to='core.FaqSection'),
            preserve_default=True,
        ),
    ]
