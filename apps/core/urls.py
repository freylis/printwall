from django.conf.urls import patterns, url

from .views import ContactView, FaqSectionView, ReviewsView


urlpatterns = patterns('',
    url(r'^contact/$', ContactView.as_view(), name='contact'),
    url(r'^faq/$', FaqSectionView.as_view(), name='faq'),
    url(r'^reviews/$', ReviewsView.as_view(), name='reviews'),

    url(r'^feedback_img_upload/$', 'apps.core.views.feedback_img_upload', name='feedback_img_upload'),
    url(r'^feedback_img_delete/(?P<pk>\d+)/$', 'apps.core.views.feedback_img_delete', name='feedback_img_delete'),
    url(r'^feedback_save/$', 'apps.core.views.feedback_save', name='feedback_save'),
    url(r'^reviews_img_upload/$', 'apps.core.views.reviews_img_upload', name='reviews_img_upload'),
    url(r'^reviews_img_delete/(?P<pk>\d+)/$', 'apps.core.views.reviews_img_delete', name='reviews_img_delete'),
    url(r'^reviews_save/$', 'apps.core.views.reviews_save', name='reviews_save'),
)
