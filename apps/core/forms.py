# -*- coding:utf-8 -*-
from django import forms

from .models import Feedback, FeedbackPhoto, Reviews, ReviewsPhoto


class FeedbackForm(forms.ModelForm):

    class Meta:
        model = Feedback


class FeedbackPhotoForm(forms.ModelForm):

    class Meta:
        model = FeedbackPhoto
        exclude = ('feedback',)


class ReviewsForm(forms.ModelForm):

    class Meta:
        model = Reviews
        exclude = ('publish',)


class ReviewsPhotoForm(forms.ModelForm):

    class Meta:
        model = ReviewsPhoto
        exclude = ('review',)
