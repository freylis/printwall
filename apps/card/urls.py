from django.conf.urls import patterns, url

urlpatterns = patterns('',

    url(r'^add$', 'apps.card.api.add_to_card', name='add_to_card'),

)
