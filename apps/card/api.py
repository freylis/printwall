# coding: utf-8
import json

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from apps.project.models import Project
from apps.catalog.models import get_item_model


@csrf_exempt
def add_to_card(request):
    request_body = json.loads(request.body)
    request_data = request_body.get('data')
    content_type_model_slug = request_data.get('initialOptions').get('content_type')
    content_type = get_item_model(content_type_model_slug).get_content_type()
    content_object_id = request_data.get('initialOptions').get('object_id')
    Project.objects.create(**{
        'content_type': content_type,
        'object_id': content_object_id,
        'data': request_data,
    })
    return JsonResponse({
        'status': True,
    })