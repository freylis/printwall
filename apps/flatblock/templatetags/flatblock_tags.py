# coding: utf-8
from django import template

from apps.flatblock.models import Flatblock

register = template.Library()


@register.inclusion_tag("flatblock/flatblock.html")
def flatblock(slug):
    block = Flatblock.get_by_slug(slug)
    return {
        "block": block,
    }


@register.inclusion_tag("flatblock/flatblock.html")
def context_flatblock(*slug_args):

    slug = "-".join(map(str, slug_args))
    block = Flatblock.get_by_slug(slug)

    return {
        "block": block,
    }

