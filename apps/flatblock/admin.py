# coding: utf-8

from django.contrib import admin

from apps.flatblock.models import Flatblock
from apps.flatblock.forms import FlatblockAdminForm


class FlatblockAdmin(admin.ModelAdmin):
    list_display = ("title", "slug", 'parent')
    readonly_fields = ("slug",)
    form = FlatblockAdminForm
admin.site.register(Flatblock, FlatblockAdmin)