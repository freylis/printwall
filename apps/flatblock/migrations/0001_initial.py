# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields
import printwall.utils
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Flatblock',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('image', sorl.thumbnail.fields.ImageField(upload_to=printwall.utils.random_path, null=True, verbose_name='\u0424\u043e\u043d', blank=True)),
                ('slug', models.SlugField(unique=True, null=True, verbose_name='\u0410\u043b\u0438\u0430\u0441')),
                ('body', tinymce.models.HTMLField(null=True, verbose_name='\u0421\u043e\u0434\u0435\u0440\u0436\u0430\u043d\u0438\u0435', blank=True)),
                ('display', models.CharField(max_length=255, null=True, verbose_name='\u0421\u043f\u043e\u0441\u043e\u0431 \u043e\u0442\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f', choices=[(b'article', '\u0421\u0442\u0430\u0442\u044c\u044f \u0441 \u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043a\u043e\u043c'), (b'border', '\u0411\u043b\u043e\u043a \u0441 \u043e\u043a\u043e\u043d\u0442\u043e\u0432\u043a\u043e\u0439'), (b'plain', '\u0411\u0435\u0437 \u043e\u0444\u043e\u0440\u043c\u043b\u0435\u043d\u0438\u044f')])),
                ('parent', models.ForeignKey(related_name=b'flatblock_flatblock', verbose_name='\u041a\u043e\u043f\u0438\u044f \u0431\u043b\u043e\u043a\u0430', blank=True, to='flatblock.Flatblock', null=True)),
            ],
            options={
                'verbose_name': '\u0411\u043b\u043e\u043a \u0442\u0435\u043a\u0441\u0442\u0430',
                'verbose_name_plural': '\u0411\u043b\u043e\u043a\u0438 \u0442\u0435\u043a\u0441\u0442\u0430',
            },
            bases=(models.Model,),
        ),
    ]
