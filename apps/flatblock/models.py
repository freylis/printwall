# coding: utf-8

from django.db import models

from tinymce.models import HTMLField
from sorl.thumbnail import ImageField

from printwall.utils import random_path


class Flatblock(models.Model):

    ARTICLE = "article"
    BORDERED = "border"
    PLAIN = "plain"

    DISPLAY_CHOICES = (
        (ARTICLE, u"Статья с заголовком"),
        (BORDERED, u"Блок с оконтовкой"),
        (PLAIN, u"Без оформления"),
    )

    class Meta:
        verbose_name = u"Блок текста"
        verbose_name_plural = u"Блоки текста"

    title = models.CharField(u"Название", null=True, blank=False, max_length=255)
    image = ImageField(u"Фон", null=True, blank=True, upload_to=random_path)
    slug = models.SlugField(u"Алиас", null=True, blank=False, unique=True)
    body = HTMLField(u"Содержание", null=True, blank=True)
    parent = models.ForeignKey("self", null=True, blank=True, verbose_name=u"Копия блока",
                                  related_name="flatblock_flatblock")
    display = models.CharField(u"Способ отображения", null=True, blank=False, choices=DISPLAY_CHOICES,
                               max_length=255)

    def __unicode__(self):
        return self.title or self.slug

    @classmethod
    def get_by_slug(cls, slug):

        try:
            block = cls.objects.get(slug=slug)
        except cls.DoesNotExist:
            block = cls.objects.create(slug=slug)
        except cls.MultipleObjectsReturned:
            block = None

        if (block is not None) and (block.parent is not None):
            block = block.parent
        return block

    def get_title(self):
        return self.title

    def get_body(self):
        return self.body or ""