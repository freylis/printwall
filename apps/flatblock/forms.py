# coding: utf-8

from django import forms

from apps.flatblock.models import Flatblock


class FlatblockAdminForm(forms.ModelForm):

    class Meta:
        model = Flatblock

    def __init__(self, *args, **kwargs):
        super(FlatblockAdminForm, self).__init__(*args, **kwargs)

        instance_id = self.instance.pk
        if instance_id is not None:
            self.fields["parent"].choices = [('', '---')] + [(block.id, block.__unicode__()) for block in
                                                             Flatblock.objects.all().exclude(id=instance_id)]