# coding: utf-8

from django.db import models
from django.core.urlresolvers import reverse

from tinymce.models import HTMLField
from sorl.thumbnail import ImageField

from printwall.utils import random_path
from printwall.models import BaseMoveModel


class BaseArticle(BaseMoveModel):

    class Meta:
        abstract = True

    title = models.CharField(u"Заголовок", null=True, blank=False, max_length=255)
    image = ImageField(u"Изображение", null=True, blank=True, upload_to=random_path)
    description = HTMLField(u"Краткое описание", null=True, blank=True)
    body = HTMLField(u"Содержание", null=True, blank=True)
    created_date = models.DateTimeField(u"Дата создания", auto_now_add=True)
    updated_date = models.DateTimeField(u"Дата обновления", auto_now=True)

    def __unicode__(self):
        return self.title

    def get_title(self):
        return self.title

    def get_url(self):
        raise NotImplementedError

    def get_description(self):
        return self.description or ""

    def get_body(self):
        return self.body or ""


class MustKnowArticle(BaseArticle):

    class Meta:
        verbose_name = u"Статья 'Это важно знать'"
        verbose_name_plural = u"Статьи 'Это важно знать'"

    def get_url(self):
        return reverse("must_know_by_id", args=[self.id])


class ItemArticle(BaseArticle):

    class Meta:
        verbose_name = u"Статья изделий"
        verbose_name_plural = u"Статьи изделий"

    show_for = models.ForeignKey('catalog.ItemType', null=True, blank=False, related_name='itemarticle_show_for',
                                 verbose_name=u'Отображать для')

    def get_url(self):
        return reverse("article_by_id", args=[self.id])


class MustKnowBlock(models.Model):

    class Meta:
        verbose_name = u"Блок 'Это важно знать'"
        verbose_name_plural = u"Блоки 'Это важно знать'"

    title = models.CharField(u"Название", null=True, blank=False, max_length=255)
    image = ImageField(u"Изображение", upload_to=random_path)
    article = models.ManyToManyField("article.MustKnowArticle", null=True, blank=False)
    order = models.IntegerField(u"Порядковый номер", null=True, blank=False, default=1)
    show_for = models.ForeignKey('catalog.ItemType', null=True, blank=False, related_name='mustknowblock_show_for',
                                 verbose_name=u'Отображать для')

    def __unicode__(self):
        return self.title


class Page(BaseArticle):

    FIRST = "first"
    SECOND = "second"
    THIRD = "third"

    POSITIONS = (
        (FIRST, u"Первое меню"),
        (SECOND, u"Второе меню"),
        (THIRD, u"Третье меню"),
    )

    class Meta:
        verbose_name = u"Инaформация"
        verbose_name_plural = u"Информация"

    position = models.CharField(u"Расположение", null=True, blank=True, max_length=10, choices=POSITIONS)

    def get_url(self):
        return reverse("page_by_id", args=[self.id])


class News(models.Model):
    name = models.CharField(u'Название', max_length=100)
    description = HTMLField(u'Описание')
    slug = models.SlugField(u'ЧПУ', unique=True)
    created = models.DateTimeField(u'Дата создания', auto_now_add=True)

    class Meta:
        verbose_name = u'Новость'
        verbose_name_plural = u'Новости'
        ordering = ('-created',)

    def get_absolute_url(self):
        return reverse('news_detail', args=[self.slug])

    def __unicode__(self):
        return self.name


class NewsImage(models.Model):
    news = models.ForeignKey(News, verbose_name=u'Новость', related_name='images')
    image = ImageField(u'Изображение', upload_to=random_path)

    class Meta:
        verbose_name = u'Изображение'
        verbose_name_plural = u'Изображения'

    def __unicode__(self):
        return str(self.pk)


class NewsSubscribe(models.Model):
    name = models.CharField(u'Имя', max_length=50, blank=True)
    email = models.EmailField(u'Ваш E-mail')

    class Meta:
        verbose_name = verbose_name_plural = u'Подписчики на новости'

    def __unicode__(self):
        return self.email
