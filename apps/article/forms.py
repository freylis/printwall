# -*- coding:utf-8 -*-
from django import forms

from .models import NewsSubscribe


class NewsSubscribeForm(forms.ModelForm):

    class Meta:
        model = NewsSubscribe
