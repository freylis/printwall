# coding: utf-8

from django.contrib import admin

from apps.article.models import MustKnowArticle, MustKnowBlock, ItemArticle,\
                                Page, News, NewsImage, NewsSubscribe
from sorl.thumbnail.admin import AdminImageMixin


class BaseArticleAdmin(admin.ModelAdmin):
    list_display = ("title", "created_date", "updated_date")
    list_filter = ("created_date", "updated_date")
    search_fields = ("title", "description", "body")


class MustKnowArticleAdmin(BaseArticleAdmin):
    pass
admin.site.register(MustKnowArticle, MustKnowArticleAdmin)


class MustKnowBlockAdmin(admin.ModelAdmin):
    list_display = ("title", "order")
    list_editable = ("order",)
admin.site.register(MustKnowBlock, MustKnowBlockAdmin)


class ItemArticleAdmin(BaseArticleAdmin):
    pass
admin.site.register(ItemArticle, ItemArticleAdmin)


class PageAdmin(BaseArticleAdmin):
    list_display = ("title", "position", "created_date", "updated_date")
    list_filter = ("position", "created_date", "updated_date")
admin.site.register(Page, PageAdmin)


class NewsImageAdminInline(AdminImageMixin, admin.TabularInline):
    model = NewsImage
    extra = 1


class NewsAdmin(admin.ModelAdmin):
    list_display = ('name', 'created')
    list_filter = ('created',)
    date_hierarchy = 'created'
    prepopulated_fields = {'slug': ('name',)}
    search_fields = ('name',)
    inlines = (NewsImageAdminInline,)

admin.site.register(News, NewsAdmin)


class NewsSubscribeAdmin(admin.ModelAdmin):
    list_display = ('email', 'name')

    def has_add_permission(self, request):
        return

admin.site.register(NewsSubscribe, NewsSubscribeAdmin)
