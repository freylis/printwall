# coding: utf-8
from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView, DetailView
from django.http import JsonResponse, Http404

from .models import ItemArticle, MustKnowArticle, Page, News, NewsSubscribe
from .forms import NewsSubscribeForm
from pure_pagination.mixins import PaginationMixin
from apps.catalog.views import ON_PAGE_DEFAULT, ON_PAGE_VARIANTS


def article_by_id(request, article_id):

    article = get_object_or_404(ItemArticle, id=article_id)

    return render(request, "article/article_by_id.html", {
        "article": article,
    })


def must_know_by_id(request, article_id):

    article = get_object_or_404(MustKnowArticle, id=article_id)

    return render(request, "article/article_by_id.html", {
        "article": article,
    })


def page_by_id(request, article_id):

    article = get_object_or_404(Page, id=article_id)

    return render(request, "article/article_by_id.html", {
        "article": article,
    })


class NewsListView(PaginationMixin, ListView):
    model = News
    paginate_by = ON_PAGE_DEFAULT

    def get_paginate_by(self, *args, **kwargs):
        paginate = int(self.request.GET.get('paginate', self.paginate_by))
        if paginate in ON_PAGE_VARIANTS:
            return paginate
        return self.paginate_by

    def get_context_data(self, **kwargs):
        ctx = super(NewsListView, self).get_context_data(**kwargs)
        ctx['form'] = NewsSubscribeForm()
        ctx['on_page_variants'] = ON_PAGE_VARIANTS
        ctx['on_page'] = self.get_paginate_by()
        return ctx


class NewsDetailView(DetailView):
    model = News


def ajax_subscribe(request):
    if request.is_ajax and request.method == 'POST':
        form = NewsSubscribeForm(request.POST)
        if form.is_valid():
            if request.POST.get('save'):
                try:
                    NewsSubscribe.objects.get(email=form.cleaned_data.get('email'))\
                                         .delete()
                except NewsSubscribe.DoesNotExist:
                    form.save()
            data = {'success': True}
            data['subscribe'] = True
            if NewsSubscribe.objects.filter(email=form.cleaned_data.get('email')).exists():
                data['subscribe'] = False
        else:
            data = {}
            data['form_errors'] = form.errors
        return JsonResponse(data)

    raise Http404
