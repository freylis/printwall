# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0001_initial'),
        ('article', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='mustknowblock',
            name='show_for',
            field=models.ForeignKey(related_name=b'mustknowblock_show_for', verbose_name='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u044c \u0434\u043b\u044f', to='catalog.ItemType', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='itemarticle',
            name='show_for',
            field=models.ForeignKey(related_name=b'itemarticle_show_for', verbose_name='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u044c \u0434\u043b\u044f', to='catalog.ItemType', null=True),
            preserve_default=True,
        ),
    ]
