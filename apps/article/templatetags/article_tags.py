# coding: utf-8

from django import template

from apps.article.models import Page

register = template.Library()


@register.simple_tag(takes_context=True)
def get_all_pages(context):
    """
    получить все информационные страницы, которые учавствуют в меню
    и отдать их в контекст
    :param context:
    :return:
    """
    all_pages = Page.objects.filter(position__in=[Page.FIRST, Page.SECOND, Page.THIRD])

    # regroup all information pages
    pages = {}
    for page in all_pages:
        if page.position not in pages.keys():
            pages[page.position] = [page]
        else:
            pages[page.position].append(page)

    # add regrouped pages to context
    context["information_pages"] = pages
    return ""


@register.inclusion_tag("includes/first_footer_menu.html", takes_context=True)
def first_footer_menu(context):
    return {
        "pages": (context.get("information_pages") or {}).get(Page.FIRST),
    }


@register.inclusion_tag("includes/second_footer_menu.html", takes_context=True)
def second_footer_menu(context):
    return {
        "pages": (context.get("information_pages") or {}).get(Page.SECOND),
    }


@register.inclusion_tag("includes/third_footer_menu.html", takes_context=True)
def third_footer_menu(context):
    return {
        "pages": (context.get("information_pages") or {}).get(Page.THIRD),
    }
