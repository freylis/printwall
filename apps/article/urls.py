from django.conf.urls import patterns, url

from .views import NewsListView, NewsDetailView

urlpatterns = patterns('',
    url(r'^news/$', NewsListView.as_view(), name='news_list'),
    url(r'^news/(?P<slug>[\-\w]+)/$', NewsDetailView.as_view(), name='news_detail'),

    url(r'^article/(?P<article_id>\d+)/$', 'apps.article.views.article_by_id', name='article_by_id'),
    url(r'^article/interesting/(?P<article_id>\d+)/$', 'apps.article.views.must_know_by_id', name='must_know_by_id'),
    url(r'^article/info/(?P<article_id>\d+)/$', 'apps.article.views.page_by_id', name='page_by_id'),

    url(r'^ajax/ajax_subscribe/$', 'apps.article.views.ajax_subscribe', name='ajax_subscribe'),
)
