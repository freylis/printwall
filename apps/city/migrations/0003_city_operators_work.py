# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('city', '0002_city_email'),
    ]

    operations = [
        migrations.AddField(
            model_name='city',
            name='operators_work',
            field=tinymce.models.HTMLField(default='', verbose_name='\u0420\u0435\u0436\u0438\u043c \u0440\u0430\u0431\u043e\u0442\u044b \u043e\u043f\u0435\u0440\u0430\u0442\u043e\u0440\u043e\u0432'),
            preserve_default=False,
        ),
    ]
