# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('city', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='city',
            name='email',
            field=models.EmailField(max_length=75, verbose_name=b'E-mail', blank=True),
            preserve_default=True,
        ),
    ]
