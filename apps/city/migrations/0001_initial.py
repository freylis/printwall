# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('is_default', models.BooleanField(default=False, verbose_name='\u0413\u043e\u0440\u043e\u0434 \u043f\u043e \u0443\u043c\u043e\u043b\u0447\u0430\u043d\u0438\u044e')),
                ('address', tinymce.models.HTMLField(null=True, verbose_name='\u0410\u0434\u0440\u0435\u0441')),
                ('phone', models.CharField(max_length=255, null=True, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d')),
                ('contacts', tinymce.models.HTMLField(null=True, verbose_name='\u041a\u043e\u043d\u0442\u0430\u043a\u0442\u044b')),
                ('work', tinymce.models.HTMLField(null=True, verbose_name='\u0420\u0435\u0436\u0438\u043c \u0440\u0430\u0431\u043e\u0442\u044b')),
                ('latitude', models.DecimalField(null=True, max_digits=9, decimal_places=6, blank=True)),
                ('longitude', models.DecimalField(null=True, max_digits=9, decimal_places=6, blank=True)),
            ],
            options={
                'ordering': ('title',),
                'verbose_name': '\u0413\u043e\u0440\u043e\u0434',
                'verbose_name_plural': '\u0413\u043e\u0440\u043e\u0434\u0430',
            },
            bases=(models.Model,),
        ),
    ]
