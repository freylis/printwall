# coding: utf-8

from django import template
from apps.city.models import City

register = template.Library()


@register.inclusion_tag("includes/cities_list.html", takes_context=True)
def get_all_cities(context):
    """
    get all available cities
    :return: cities list
    """

    cities = City.objects.all()
    current_url = context["request"].get_full_path()

    return {
        "cities": cities,
        "current_url": current_url,
    }