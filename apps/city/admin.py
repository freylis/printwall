# coding: utf-8

from django.contrib import admin

from apps.city.models import City
from pygeocoder import Geocoder, GeocoderError


class CityAdmin(admin.ModelAdmin):
    list_display = ("title", "is_default")

    def save_model(self, request, obj, form, change):
        if not obj.latitude or not obj.longitude:
            try:
                address = obj.address
                if not obj.title in address:
                    address = '%s %s' % (obj.title, address)
                results = Geocoder.geocode(address)
                results = results[0].coordinates
                obj.latitude = results[0]
                obj.longitude = results[1]
            except GeocoderError:
                pass
        obj.save()

admin.site.register(City, CityAdmin)
