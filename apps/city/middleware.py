from apps.city.models import City
from django.utils.functional import SimpleLazyObject


class CurrentCityMiddleware(object):

    def current_city(self, request):
        """
        get city, nearest to coordinates, get by geoip or default city
        """
        city = None

        # in first - search city in session
        city_id = request.session.get("current_city")
        if city_id is not None:
            try:
                city = City.objects.get(id=city_id)
            except City.DoesNotExist:
                pass

        if city is None:

            location = request.location
            if isinstance(location, SimpleLazyObject):
                try:
                    city = City.objects.get(is_default=True)
                except City.DoesNotExist:
                    city = None

        return city

    def process_request(self, request):
        request.current_city = self.current_city(request)
