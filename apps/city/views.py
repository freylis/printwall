# coding: utf-8

from django.shortcuts import redirect
from django.core.urlresolvers import reverse

from apps.city.models import City


def set_city(request, city_id):
    """
    set city in session and redirect to next page
    :param request:
    :return: 302
    """

    city = None

    try:
        city = City.objects.get(id=city_id)
    except City.DoesNotExist:
        try:
            city = City.objects.get(is_default=True)
        except City.DoesNotExist:
            pass

    if city is not None:
        request.session["current_city"] = city.id

    next_page = request.GET.get("next") or reverse("home")
    return redirect(next_page)

