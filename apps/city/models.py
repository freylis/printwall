# coding: utf-8

from django.db import models
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse

from tinymce.models import HTMLField


class City(models.Model):

    class Meta:
        verbose_name = u"Город"
        verbose_name_plural = u"Города"
        ordering = ("title",)

    title = models.CharField(u"Название", null=True, blank=False, max_length=255)
    is_default = models.BooleanField(u"Город по умолчанию", blank=True, default=False)
    address = HTMLField(u"Адрес", null=True, blank=False)
    phone = models.CharField(u"Телефон", null=True, blank=False, max_length=255)
    email = models.EmailField('E-mail', blank=True)
    contacts = HTMLField(u"Контакты", null=True, blank=False)
    work = HTMLField(u"Режим работы", null=True, blank=False)
    operators_work = HTMLField(u"Режим работы операторов")
    latitude = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)
    longitude = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)

    def __unicode__(self):
        return self.title

    def clean(self):

        if self.is_default:
            another_default_cities = self.__class__.objects.filter(is_default=True)
            if self.id is not None:
                another_default_cities = another_default_cities.exclude(id=self.id)
            if another_default_cities.exists():
                raise ValidationError(u"Может быть только одн город по умолчанию")

    def get_title(self):
        return self.title

    def get_coordinates(self):
        return '%s, %s' % (float(self.latitude), float(self.longitude))

    def get_url(self):
        return reverse("set_city", args=[self.id])
