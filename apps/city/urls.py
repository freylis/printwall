from django.conf.urls import patterns, url

urlpatterns = patterns('',

    url(r'^(?P<city_id>\d+)/set/$', 'apps.city.views.set_city', name='set_city'),

)
