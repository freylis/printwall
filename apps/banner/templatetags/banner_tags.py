# coding: utf-8
from django import template

from apps.banner.models import Banner

register = template.Library()


@register.inclusion_tag("banner/random_banner.html")
def banner():

    return {
        "banner": Banner.get_random_banner(),
    }

