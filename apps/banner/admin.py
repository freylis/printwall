# coding: utf-8

from django.contrib import admin

from apps.banner.models import Banner


class BannerAdmin(admin.ModelAdmin):
    pass
admin.site.register(Banner, BannerAdmin)