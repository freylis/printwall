# coding: utf-8
import random

from django.db import models

from sorl.thumbnail import ImageField

from printwall.utils import random_path


class Banner(models.Model):

    class Meta:
        verbose_name = u"Банер"
        verbose_name_plural = u"Банеры"

    title = models.CharField(u"Название", null=True, blank=False, max_length=255)
    image = ImageField(u"Изображение", null=True, blank=False, upload_to=random_path)

    def __unicode__(self):
        return self.title

    @classmethod
    def get_random_banner(cls):

        # get all banners ids
        banner_id_list = cls.objects.all().values_list("id", flat=True)

        # from all ids get random id
        try:
            random_banner_id = random.choice(banner_id_list)
        except IndexError:
            return None

        # and by random id - get banner
        try:
            banner = cls.objects.get(id=random_banner_id)
        except cls.DoesNotExist:
            return None

        return banner