# coding: utf-8

from django.contrib import admin

from apps.options.models import Size, Texture


class SizeAdmin(admin.ModelAdmin):
    pass
admin.site.register(Size, SizeAdmin)


class TextureAdmin(admin.ModelAdmin):
    pass
admin.site.register(Texture, TextureAdmin)