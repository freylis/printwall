# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import printwall.utils


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Size',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('width', models.PositiveIntegerField(null=True, verbose_name='\u0428\u0438\u0440\u0438\u043d\u0430')),
                ('height', models.PositiveIntegerField(null=True, verbose_name='\u0412\u044b\u0441\u043e\u0442\u0430')),
                ('display_to', models.ManyToManyField(related_name=b'size_display_to', null=True, verbose_name='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u044c \u0434\u043b\u044f', to='catalog.ItemType')),
            ],
            options={
                'verbose_name': '\u0420\u0430\u0437\u043c\u0435\u0440',
                'verbose_name_plural': '\u0420\u0430\u0437\u043c\u0435\u0440\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Texture',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('texture', models.ImageField(upload_to=printwall.utils.random_path, null=True, verbose_name='\u0422\u0435\u043a\u0441\u0442\u0443\u0440\u0430', blank=True)),
                ('available_for', models.CharField(max_length=20, null=True, verbose_name='\u0414\u043e\u0441\u0442\u0443\u043f\u043d\u0430 \u0434\u043b\u044f', choices=[(b'ff', '\u0424\u0440\u0435\u0441\u043a\u0438 \u043d\u0430 \u0444\u043b\u0435\u0437\u0435\u043b\u0438\u043d\u043e\u0432\u043e\u0439 \u043e\u0441\u043d\u043e\u0432\u0435'), (b'fb', '\u0424\u0440\u0435\u0441\u043a\u0438 \u0431\u043e\u043b\u044c\u0448\u0438\u0445 \u0440\u0430\u0437\u043c\u0435\u0440\u043e\u0432')])),
                ('price', models.PositiveIntegerField(default=0, null=True, verbose_name='\u0426\u0435\u043d\u0430 (\u043a\u0432.\u043c)')),
            ],
            options={
                'verbose_name': '\u0424\u0430\u043a\u0442\u0443\u0440\u0430',
                'verbose_name_plural': '\u0424\u0430\u043a\u0442\u0443\u0440\u044b',
            },
            bases=(models.Model,),
        ),
    ]
