# coding: utf-8

from apps.options.models import Texture


def texture_type(request):
    return {
        'FRESCO_F': Texture.FRESCO_F,
        'FRESCO_B': Texture.FRESCO_B,
    }

