# coding: utf-8

from django.db import models

from sorl.thumbnail import ImageField

from printwall.utils import random_path


class Size(models.Model):

    class Meta:
        verbose_name = u'Размер'
        verbose_name_plural = u'Размеры'

    width = models.PositiveIntegerField(u'Ширина', null=True, blank=False)
    height = models.PositiveIntegerField(u'Высота', null=True, blank=False)
    display_to = models.ManyToManyField('catalog.ItemType', null=True, blank=False, related_name='size_display_to',
                                        verbose_name=u'Отображать для')

    def __unicode__(self):
        return '%dx%d' % (self.width, self.height)


class Texture(models.Model):

    FRESCO_F = 'ff'
    FRESCO_B = 'fb'
    WALLPAPER = 'w'
    PICTURE = 'pic'
    POSTER = 'poster'
    MOSAIC = 'm'
    ITEM_TYPES = (
        (FRESCO_F, u'Фрески на флезелиновой основе'),
        (FRESCO_B, u'Фрески больших размеров'),
    )
    ITEM_TYPE_VARIANTS = [item_type[0] for item_type in ITEM_TYPES]

    class Meta:
        verbose_name = u'Фактура'
        verbose_name_plural = u'Фактуры'

    name = models.CharField(u'Название', null=True, blank=False, max_length=255)
    texture = models.ImageField(u'Текстура', null=True, blank=True, upload_to=random_path)
    available_for = models.CharField(u'Доступна для', null=True, blank=False, max_length=20,
                                     choices=ITEM_TYPES)
    price = models.PositiveIntegerField(u'Цена (кв.м)', null=True, blank=False, default=0)

    def __unicode__(self):
        return self.name

    def get_title(self):
        return self.name
