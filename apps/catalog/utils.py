# coding: utf-8
from django.shortcuts import Http404

from apps.catalog.models import get_item_model
from apps.options.models import Texture


def get_item(item_type, item_id):
    """
    get item instance or None
    :param item_type: model slug
    :param item_id: instance id
    :return: item
    """

    model = get_item_model(item_type)
    if model is not None:
        try:
            instance = model.objects.get(id=item_id)
        except model.DoesNotExist:
            pass
        else:
            return instance
    return None


def paginator_befor_after(paginator, on_side=3):
    pages_befor_list = []
    pages_after_list = []
    for ind in range(paginator.number-on_side, paginator.number+on_side+1):
        if ind <= paginator.paginator.num_pages and ind >= 1:
            if ind < paginator.number:
                pages_befor_list.append(ind)
            if ind > paginator.number:
                pages_after_list.append(ind)
    paginator.befor_list = pages_befor_list
    paginator.after_list = pages_after_list
    return paginator



def get_texture_type_by_item_model(item_model, texture_key):

    texture_key = texture_key or item_model.TEXTURE_TYPE
    if texture_key in Texture.ITEM_TYPE_VARIANTS:
        return Texture.objects.filter(available_for=texture_key)
    raise Http404
