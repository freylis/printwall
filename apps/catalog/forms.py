# coding: utf-8

from django import forms
from django.contrib.contenttypes.models import ContentType

from apps.catalog.models import ItemType, ITEM_MODELS


class ItemAdminForm(forms.ModelForm):

    class Meta:
        model = ItemType

    def __init__(self, *args, **kwargs):
        super(ItemAdminForm, self).__init__(*args, **kwargs)

        app_label = "catalog"
        available_models = [model._meta.module_name for model in ITEM_MODELS.values()]

        available_ctypes = ContentType.objects.filter(app_label=app_label,
                                                      model__in=available_models).values_list("id", "name")
        self.fields["ctype"].choices = available_ctypes


class GoodsAdminForm(forms.ModelForm):

    def clean(self):
        cleaned_data = super(GoodsAdminForm, self).clean()
        exist = cleaned_data.get('exist')
        sale = cleaned_data.get('sale')

        if sale and not exist:
            raise forms.ValidationError(u'Для того, чтобы указать распродажу,\
                                         объект должен быть в наличии')
