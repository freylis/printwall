# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields
import mptt.fields
import tinymce.models
import printwall.utils


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('created_date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('updated_date', models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u043e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u0438\u044f')),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('parent', mptt.fields.TreeForeignKey(related_name=b'children', verbose_name='\u0420\u043e\u0434\u0438\u0442\u0435\u043b\u044c', blank=True, to='catalog.Category', null=True)),
            ],
            options={
                'verbose_name': '\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f',
                'verbose_name_plural': '\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Fresco',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.IntegerField(default=1, null=True, editable=False)),
                ('title', models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('image', sorl.thumbnail.fields.ImageField(upload_to=printwall.utils.random_path, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('price', models.DecimalField(null=True, verbose_name='\u0426\u0435\u043d\u0430', max_digits=7, decimal_places=2)),
                ('popular', models.BooleanField(default=False, verbose_name='\u041f\u043e\u043f\u0443\u043b\u044f\u0440\u043d\u043e\u0435')),
                ('exist', models.BooleanField(default=True, verbose_name='\u0412 \u043d\u0430\u043b\u0438\u0447\u0438\u0438')),
                ('discount', models.BooleanField(default=True, verbose_name='\u0421\u043a\u0438\u0434\u043a\u0430')),
                ('view', models.CharField(blank=True, max_length=20, null=True, verbose_name='\u0420\u0430\u0441\u043f\u043e\u043b\u043e\u0436\u0435\u043d\u0438\u0435', choices=[(b'horizontal', '\u0413\u043e\u0440\u0438\u0437\u043e\u043d\u0442\u0430\u043b\u044c\u043d\u044b\u0435'), (b'square', '\u041a\u0432\u0430\u0434\u0440\u0430\u0442\u043d\u044b\u0435'), (b'vertical', '\u0412\u0435\u0440\u0442\u0438\u043a\u0430\u043b\u044c\u043d\u044b\u0435')])),
                ('creating', models.CharField(default='\u0438\u0437\u0433\u043e\u0442\u043e\u0432\u043b\u0435\u043d\u0438\u0435 2-3 \u0434\u043d\u044f', max_length=50, null=True, verbose_name='\u0418\u0437\u0433\u043e\u0442\u043e\u0432\u043b\u0435\u043d\u0438\u0435', help_text='\u0438\u0437\u0433\u043e\u0442\u043e\u0432\u043b\u0435\u043d\u0438\u0435 2-3 \u0434\u043d\u044f')),
                ('at_main', models.BooleanField(default=False, verbose_name='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u044c \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439')),
                ('created_date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('updated_date', models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u043e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u0438\u044f')),
                ('category', models.ManyToManyField(to='catalog.Category', null=True, verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438', blank=True)),
            ],
            options={
                'verbose_name': '\u0424\u0440\u0435\u0441\u043a\u0430',
                'verbose_name_plural': '\u0424\u0440\u0435\u0441\u043a\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FrescoImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', sorl.thumbnail.fields.ImageField(upload_to=printwall.utils.random_path, null=True, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('title', models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('item', models.ForeignKey(to='catalog.Fresco')),
            ],
            options={
                'verbose_name': '\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u0444\u0440\u0435\u0441\u043e\u043a',
                'verbose_name_plural': '\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f \u0444\u0440\u0435\u0441\u043e\u043a',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ItemType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.IntegerField(default=1, null=True, editable=False)),
                ('title', models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('main_title', models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0434\u043b\u044f \u0433\u043b\u0430\u0432\u043d\u043e\u0439 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b')),
                ('image', sorl.thumbnail.fields.ImageField(upload_to=printwall.utils.random_path, null=True, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439')),
                ('description', tinymce.models.HTMLField(null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('max_width', models.PositiveIntegerField(null=True, verbose_name='\u041c\u0430\u043a\u0441\u0438\u043c\u0430\u043b\u044c\u043d\u0430\u044f \u0448\u0438\u0440\u0438\u043d\u0430')),
                ('max_height', models.PositiveIntegerField(null=True, verbose_name='\u041c\u0430\u043a\u0441\u0438\u043c\u0430\u043b\u044c\u043d\u0430\u044f \u0432\u044b\u0441\u043e\u0442\u0430')),
                ('enable', models.BooleanField(default=True, verbose_name='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u044c')),
                ('ctype', models.ForeignKey(null=True, verbose_name='\u0422\u0438\u043f \u0441\u043e\u0434\u0435\u0440\u0436\u0438\u043c\u043e\u0433\u043e', to='contenttypes.ContentType', unique=True)),
            ],
            options={
                'verbose_name': '\u0422\u0438\u043f \u0438\u0437\u0434\u0435\u043b\u0438\u044f',
                'verbose_name_plural': '\u0422\u0438\u043f\u044b \u0438\u0437\u0434\u0435\u043b\u0438\u0439',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Mosaic',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.IntegerField(default=1, null=True, editable=False)),
                ('title', models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('image', sorl.thumbnail.fields.ImageField(upload_to=printwall.utils.random_path, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('price', models.DecimalField(null=True, verbose_name='\u0426\u0435\u043d\u0430', max_digits=7, decimal_places=2)),
                ('popular', models.BooleanField(default=False, verbose_name='\u041f\u043e\u043f\u0443\u043b\u044f\u0440\u043d\u043e\u0435')),
                ('exist', models.BooleanField(default=True, verbose_name='\u0412 \u043d\u0430\u043b\u0438\u0447\u0438\u0438')),
                ('discount', models.BooleanField(default=True, verbose_name='\u0421\u043a\u0438\u0434\u043a\u0430')),
                ('view', models.CharField(blank=True, max_length=20, null=True, verbose_name='\u0420\u0430\u0441\u043f\u043e\u043b\u043e\u0436\u0435\u043d\u0438\u0435', choices=[(b'horizontal', '\u0413\u043e\u0440\u0438\u0437\u043e\u043d\u0442\u0430\u043b\u044c\u043d\u044b\u0435'), (b'square', '\u041a\u0432\u0430\u0434\u0440\u0430\u0442\u043d\u044b\u0435'), (b'vertical', '\u0412\u0435\u0440\u0442\u0438\u043a\u0430\u043b\u044c\u043d\u044b\u0435')])),
                ('creating', models.CharField(default='\u0438\u0437\u0433\u043e\u0442\u043e\u0432\u043b\u0435\u043d\u0438\u0435 2-3 \u0434\u043d\u044f', max_length=50, null=True, verbose_name='\u0418\u0437\u0433\u043e\u0442\u043e\u0432\u043b\u0435\u043d\u0438\u0435', help_text='\u0438\u0437\u0433\u043e\u0442\u043e\u0432\u043b\u0435\u043d\u0438\u0435 2-3 \u0434\u043d\u044f')),
                ('at_main', models.BooleanField(default=False, verbose_name='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u044c \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439')),
                ('created_date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('updated_date', models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u043e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u0438\u044f')),
                ('category', models.ManyToManyField(to='catalog.Category', null=True, verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438', blank=True)),
            ],
            options={
                'verbose_name': '\u0424\u0440\u0435\u0441\u043a\u0430-\u043c\u043e\u0437\u0430\u0439\u043a\u0430',
                'verbose_name_plural': '\u0424\u0440\u0435\u0441\u043a\u0438-\u043c\u043e\u0437\u0430\u0439\u043a\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MosaicImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', sorl.thumbnail.fields.ImageField(upload_to=printwall.utils.random_path, null=True, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('title', models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('item', models.ForeignKey(to='catalog.Mosaic')),
            ],
            options={
                'verbose_name': '\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u0444\u0440\u0435\u0441\u043a\u0438-\u043c\u043e\u0437\u0430\u0439\u043a\u0438',
                'verbose_name_plural': '\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f \u0444\u0440\u0435\u0441\u043e\u043a-\u043c\u043e\u0437\u0430\u0435\u043a',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Picture',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.IntegerField(default=1, null=True, editable=False)),
                ('title', models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('image', sorl.thumbnail.fields.ImageField(upload_to=printwall.utils.random_path, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('price', models.DecimalField(null=True, verbose_name='\u0426\u0435\u043d\u0430', max_digits=7, decimal_places=2)),
                ('popular', models.BooleanField(default=False, verbose_name='\u041f\u043e\u043f\u0443\u043b\u044f\u0440\u043d\u043e\u0435')),
                ('exist', models.BooleanField(default=True, verbose_name='\u0412 \u043d\u0430\u043b\u0438\u0447\u0438\u0438')),
                ('discount', models.BooleanField(default=True, verbose_name='\u0421\u043a\u0438\u0434\u043a\u0430')),
                ('view', models.CharField(blank=True, max_length=20, null=True, verbose_name='\u0420\u0430\u0441\u043f\u043e\u043b\u043e\u0436\u0435\u043d\u0438\u0435', choices=[(b'horizontal', '\u0413\u043e\u0440\u0438\u0437\u043e\u043d\u0442\u0430\u043b\u044c\u043d\u044b\u0435'), (b'square', '\u041a\u0432\u0430\u0434\u0440\u0430\u0442\u043d\u044b\u0435'), (b'vertical', '\u0412\u0435\u0440\u0442\u0438\u043a\u0430\u043b\u044c\u043d\u044b\u0435')])),
                ('creating', models.CharField(default='\u0438\u0437\u0433\u043e\u0442\u043e\u0432\u043b\u0435\u043d\u0438\u0435 2-3 \u0434\u043d\u044f', max_length=50, null=True, verbose_name='\u0418\u0437\u0433\u043e\u0442\u043e\u0432\u043b\u0435\u043d\u0438\u0435', help_text='\u0438\u0437\u0433\u043e\u0442\u043e\u0432\u043b\u0435\u043d\u0438\u0435 2-3 \u0434\u043d\u044f')),
                ('at_main', models.BooleanField(default=False, verbose_name='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u044c \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439')),
                ('created_date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('updated_date', models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u043e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u0438\u044f')),
                ('category', models.ManyToManyField(to='catalog.Category', null=True, verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438', blank=True)),
            ],
            options={
                'verbose_name': '\u041a\u0430\u0440\u0442\u0438\u043d\u0430',
                'verbose_name_plural': '\u041a\u0430\u0440\u0442\u0438\u043d\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PictureImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', sorl.thumbnail.fields.ImageField(upload_to=printwall.utils.random_path, null=True, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('title', models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('item', models.ForeignKey(to='catalog.Picture')),
            ],
            options={
                'verbose_name': '\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u043a\u0430\u0440\u0442\u0438\u043d\u044b',
                'verbose_name_plural': '\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f \u043a\u0430\u0440\u0442\u0438\u043d',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Poster',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.IntegerField(default=1, null=True, editable=False)),
                ('title', models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('image', sorl.thumbnail.fields.ImageField(upload_to=printwall.utils.random_path, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('price', models.DecimalField(null=True, verbose_name='\u0426\u0435\u043d\u0430', max_digits=7, decimal_places=2)),
                ('popular', models.BooleanField(default=False, verbose_name='\u041f\u043e\u043f\u0443\u043b\u044f\u0440\u043d\u043e\u0435')),
                ('exist', models.BooleanField(default=True, verbose_name='\u0412 \u043d\u0430\u043b\u0438\u0447\u0438\u0438')),
                ('discount', models.BooleanField(default=True, verbose_name='\u0421\u043a\u0438\u0434\u043a\u0430')),
                ('view', models.CharField(blank=True, max_length=20, null=True, verbose_name='\u0420\u0430\u0441\u043f\u043e\u043b\u043e\u0436\u0435\u043d\u0438\u0435', choices=[(b'horizontal', '\u0413\u043e\u0440\u0438\u0437\u043e\u043d\u0442\u0430\u043b\u044c\u043d\u044b\u0435'), (b'square', '\u041a\u0432\u0430\u0434\u0440\u0430\u0442\u043d\u044b\u0435'), (b'vertical', '\u0412\u0435\u0440\u0442\u0438\u043a\u0430\u043b\u044c\u043d\u044b\u0435')])),
                ('creating', models.CharField(default='\u0438\u0437\u0433\u043e\u0442\u043e\u0432\u043b\u0435\u043d\u0438\u0435 2-3 \u0434\u043d\u044f', max_length=50, null=True, verbose_name='\u0418\u0437\u0433\u043e\u0442\u043e\u0432\u043b\u0435\u043d\u0438\u0435', help_text='\u0438\u0437\u0433\u043e\u0442\u043e\u0432\u043b\u0435\u043d\u0438\u0435 2-3 \u0434\u043d\u044f')),
                ('at_main', models.BooleanField(default=False, verbose_name='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u044c \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439')),
                ('created_date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('updated_date', models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u043e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u0438\u044f')),
                ('category', models.ManyToManyField(to='catalog.Category', null=True, verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438', blank=True)),
            ],
            options={
                'verbose_name': '\u041f\u043e\u0441\u0442\u0435\u0440',
                'verbose_name_plural': '\u041f\u043e\u0441\u0442\u0435\u0440\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PosterImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', sorl.thumbnail.fields.ImageField(upload_to=printwall.utils.random_path, null=True, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('title', models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('item', models.ForeignKey(to='catalog.Poster')),
            ],
            options={
                'verbose_name': '\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u043f\u043e\u0441\u0442\u0435\u0440\u0430',
                'verbose_name_plural': '\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f \u043f\u043e\u0441\u0442\u0435\u0440\u043e\u0432',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Wallpaper',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.IntegerField(default=1, null=True, editable=False)),
                ('title', models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('image', sorl.thumbnail.fields.ImageField(upload_to=printwall.utils.random_path, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('price', models.DecimalField(null=True, verbose_name='\u0426\u0435\u043d\u0430', max_digits=7, decimal_places=2)),
                ('popular', models.BooleanField(default=False, verbose_name='\u041f\u043e\u043f\u0443\u043b\u044f\u0440\u043d\u043e\u0435')),
                ('exist', models.BooleanField(default=True, verbose_name='\u0412 \u043d\u0430\u043b\u0438\u0447\u0438\u0438')),
                ('discount', models.BooleanField(default=True, verbose_name='\u0421\u043a\u0438\u0434\u043a\u0430')),
                ('view', models.CharField(blank=True, max_length=20, null=True, verbose_name='\u0420\u0430\u0441\u043f\u043e\u043b\u043e\u0436\u0435\u043d\u0438\u0435', choices=[(b'horizontal', '\u0413\u043e\u0440\u0438\u0437\u043e\u043d\u0442\u0430\u043b\u044c\u043d\u044b\u0435'), (b'square', '\u041a\u0432\u0430\u0434\u0440\u0430\u0442\u043d\u044b\u0435'), (b'vertical', '\u0412\u0435\u0440\u0442\u0438\u043a\u0430\u043b\u044c\u043d\u044b\u0435')])),
                ('creating', models.CharField(default='\u0438\u0437\u0433\u043e\u0442\u043e\u0432\u043b\u0435\u043d\u0438\u0435 2-3 \u0434\u043d\u044f', max_length=50, null=True, verbose_name='\u0418\u0437\u0433\u043e\u0442\u043e\u0432\u043b\u0435\u043d\u0438\u0435', help_text='\u0438\u0437\u0433\u043e\u0442\u043e\u0432\u043b\u0435\u043d\u0438\u0435 2-3 \u0434\u043d\u044f')),
                ('at_main', models.BooleanField(default=False, verbose_name='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u044c \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439')),
                ('created_date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('updated_date', models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u043e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u0438\u044f')),
                ('category', models.ManyToManyField(to='catalog.Category', null=True, verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438', blank=True)),
            ],
            options={
                'verbose_name': '\u0424\u043e\u0442\u043e\u043e\u0431\u043e\u0438',
                'verbose_name_plural': '\u0424\u043e\u0442\u043e\u043e\u0431\u043e\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='WallpaperImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', sorl.thumbnail.fields.ImageField(upload_to=printwall.utils.random_path, null=True, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('title', models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('item', models.ForeignKey(to='catalog.Wallpaper')),
            ],
            options={
                'verbose_name': '\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u0444\u043e\u0442\u043e\u043e\u0431\u043e\u0435\u0432',
                'verbose_name_plural': '\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f \u0444\u043e\u0442\u043e\u043e\u0431\u043e\u0435\u0432',
            },
            bases=(models.Model,),
        ),
    ]
