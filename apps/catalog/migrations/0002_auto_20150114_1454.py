# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='fresco',
            name='sale',
            field=models.BooleanField(default=False, verbose_name='\u0420\u0430\u0441\u043f\u0440\u043e\u0434\u0430\u0436\u0430'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='mosaic',
            name='sale',
            field=models.BooleanField(default=False, verbose_name='\u0420\u0430\u0441\u043f\u0440\u043e\u0434\u0430\u0436\u0430'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='picture',
            name='sale',
            field=models.BooleanField(default=False, verbose_name='\u0420\u0430\u0441\u043f\u0440\u043e\u0434\u0430\u0436\u0430'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='poster',
            name='sale',
            field=models.BooleanField(default=False, verbose_name='\u0420\u0430\u0441\u043f\u0440\u043e\u0434\u0430\u0436\u0430'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='wallpaper',
            name='sale',
            field=models.BooleanField(default=False, verbose_name='\u0420\u0430\u0441\u043f\u0440\u043e\u0434\u0430\u0436\u0430'),
            preserve_default=True,
        ),
    ]
