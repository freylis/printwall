from django.conf.urls import patterns, url

from .models import Fresco
from .views import SearchVew, SaleView

urlpatterns = patterns('',

    url(r'^api/item_card/(?P<item_type>\w+)/(?P<item_id>\d+)$', 'apps.catalog.api.item_card',
        name='api_item_card'),
    url(r'^search/$', SearchVew.as_view(), name='search'),
    url(r'^sale/$', SaleView.as_view(), name='sale'),
    url(r'^(?P<item_type_slug>\w+)/$', 'apps.catalog.views.catalog_by_slug',
        name='catalog_by_slug'),
    url(r'^(?P<item_type_slug>\w+)/filter/$', 'apps.catalog.views.filter_by_category',
        name='filter_by_category'),
    url(r'^(?P<item_type_slug>\w+)/(?P<item_id>\d+)/$', "apps.catalog.views.item_by_id", {'texture_type': None},
        name="item_by_id"),
    url(r'^(?P<item_type_slug>\w+)/(?P<item_id>\d+)/choice/$', 'apps.catalog.views.choice_texture',
        name='choice_texture'),
    url(r'^%s/(?P<item_id>\d+)/(?P<texture_type>\w+)/$' % Fresco.TYPE, "apps.catalog.views.item_by_id",
        {'item_type_slug': Fresco.TYPE}, name="item_by_id_with_texture"),


)
