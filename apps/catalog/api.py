# coding: utf-8
from django.http import JsonResponse
from django.shortcuts import render

from sorl.thumbnail import get_thumbnail

from apps.catalog.utils import get_item


def item_card(request, item_type, item_id):
    """
    get json structure for item card
    :param request:
    :param item_type: - model slug (fresco, wallpaper, etc)
    :param item_id: - id of model instance
    :return: json with structure for item card
    """

    status = False

    images = []

    item = get_item(item_type, item_id)
    if item is not None:

        # additional images
        images_model = item.get_image_model()
        if images_model is not None:
            images = images_model.objects.filter(item_id=item.id)
    print 111
    return render(request, "catalog/includes/item_card.html", {
        "item": item,
        "images": images,
    })