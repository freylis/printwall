# coding: utf-8

from django.contrib import admin

from mptt.admin import MPTTModelAdmin

from apps.catalog.models import (Category, Fresco, Wallpaper, Mosaic, Poster, Picture, ItemType, FrescoImage,
                                 WallpaperImage, MosaicImage, PosterImage, PictureImage)
from apps.catalog.forms import ItemAdminForm, GoodsAdminForm


class ItemAdmin(admin.ModelAdmin):
    form = ItemAdminForm
admin.site.register(ItemType, ItemAdmin)


class CategoryAdmin(MPTTModelAdmin):
    list_display = ("title",)
admin.site.register(Category, CategoryAdmin)


class BaseImageInline(admin.TabularInline):
    extra = 3


class BaseItemAdmin(admin.ModelAdmin):
    form = GoodsAdminForm


class FrescoImageInline(BaseImageInline):
    model = FrescoImage


class FrescoAdmin(BaseItemAdmin):
    inlines = [FrescoImageInline]
admin.site.register(Fresco, FrescoAdmin)


class WallpaperImageInline(BaseImageInline):
    model = WallpaperImage


class WallpaperAdmin(BaseItemAdmin):
    inlines = [WallpaperImageInline]
admin.site.register(Wallpaper, WallpaperAdmin)


class MosaicImageInline(BaseImageInline):
    model = MosaicImage


class MosaicAdmin(BaseItemAdmin):
    inlines = [MosaicImageInline]
admin.site.register(Mosaic, MosaicAdmin)


class PosterImageInline(BaseImageInline):
    model = PosterImage


class PosterAdmin(BaseItemAdmin):
    inlines = [PosterImageInline]
admin.site.register(Poster, PosterAdmin)


class PictureImageInline(BaseImageInline):
    model = PictureImage


class PictureAdmin(BaseItemAdmin):
    inlines = [PictureImageInline]
admin.site.register(Picture, PictureAdmin)
