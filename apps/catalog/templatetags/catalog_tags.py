# coding: utf-8
from django import template
from apps.catalog.models import ItemType

register = template.Library()


@register.simple_tag(takes_context=True)
def get_main_menu_items(context):
    menu_items = ItemType.objects.filter(enable=True)
    context["menu_items"] = menu_items

    # regroup menu items it view:
    # {
    #     model_class: title_for_main,
    #     model_class: title_for_main,
    # }
    titles_for_main = {}
    for content_type in menu_items:
        titles_for_main[content_type.ctype.model_class()] = content_type.main_title
    context["titles_for_main"] = titles_for_main
    return ""


@register.inclusion_tag("includes/main_menu.html", takes_context=True)
def main_menu(context):
    return {
        'menu_items': context.get("menu_items") or [],
        "STATIC_URL": context.get("STATIC_URL") or "/static/",
    }


@register.inclusion_tag("includes/main_banners.html", takes_context=True)
def main_banners(context):
    return {
        'menu_items': context.get("menu_items") or [],
        "STATIC_URL": context.get("STATIC_URL") or "/static/",
    }


@register.simple_tag(takes_context=True)
def get_item_title_at_main_page(context, item):
    """
    возвращает title изделия для главной страницы
    :param item: instance of BaseItem children model
    :return: <title to main page>
    """

    item_class = item.__class__
    return (context.get("titles_for_main") or {}).get(item_class) or item.get_title()


@register.simple_tag(takes_context=True)
def get_p(context, param_name, param_value):
    query_data = context["request"].GET.dict()
    # заменим нужный на значение
    query_data[param_name] = param_value
    # отдадим строку
    result = '&'.join(['%s=%s' % (k,v) for k,v in query_data.items()])
    return '?%s' % result

