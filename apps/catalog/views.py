# coding: utf-8
from django.views.generic import ListView
from django.shortcuts import render, Http404
from django.contrib.contenttypes.models import ContentType
from django.utils.datastructures import SortedDict
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from apps.catalog.models import get_item_model, ItemType, Category, Fresco
from apps.catalog.utils import paginator_befor_after, get_texture_type_by_item_model
from printwall.utils import QuerySetJoin
from pure_pagination.mixins import PaginationMixin


ON_PAGE_DEFAULT = 20
ON_PAGE_VARIANTS = [ON_PAGE_DEFAULT, 40, 60]
PAGE_DEFAULT = 1


ORDER_KEYS = {
    "e": {
        "title": u"В наличии",
        "order": ("exist",),
    },
    "d": {
        "title": u"Со скидкой",
        "order": ("discount",),
    },
    "h": {
        "title": u"Горизонтальные",
        "order": ("hirisontal",),
    },
    "s": {
        "title": u"Квадратные",
        "order": ("square",),
    },
    "v": {
        "title": u"Вертикальные",
        "order": ("vertical",),
    },
}


def catalog_by_slug(request, item_type_slug):
    """
    get category items by category slug
    :param request:
    :param item_type_slug
    :return:
    """
    item_model = get_item_model(item_type_slug)
    if item_model is None:
        raise Http404

    ctype = ContentType.objects.get_for_model(item_model)

    try:
        item_type = ItemType.objects.get(ctype_id=ctype.id)
    except ItemType.DoesNotExist:
        raise Http404

    items = item_model.objects.all()

    all_item_id_list = items.values_list("id", flat=True)

    #  get all categories with `current-item-model` goods
    categories = Category.objects.all()
    category_related_name, item_field = Category.qs_by_item_type(item_type_slug)
    if (category_related_name is not None) and (item_field is not None):
        conditions = {
            "%s__in" % item_field: all_item_id_list,
        }
        categories = getattr(categories, category_related_name, Category.objects.all()).filter(**conditions)

    # regroup categories
    structure = SortedDict()
    first_level_categories = categories.filter(level=0)
    second_level_categories = categories.filter(level=1)
    for first_level in first_level_categories:
        structure[first_level] = []

    for second_level in second_level_categories:
        if second_level.parent in structure.keys():
            structure[second_level.parent].append(second_level)
        else:
            structure[second_level.parent] = [second_level]

    # item articles
    articles = item_type.itemarticle_show_for.all()

    articles_count = len(articles)

    must_know_blocks = item_type.mustknowblock_show_for.all()[:articles_count]

    articles_container = []
    for article_indx, article in enumerate(articles):
        try:
            must_know = must_know_blocks[article_indx]
        except IndexError:
            must_know = None
        articles_container.append({
            "article": article,
            "must_know": must_know,
        })

    return render(request, 'catalog/catalog_by_slug.html', {
        "item_type": item_type,
        "item_model": item_model,
        "structure": structure,
        "info_list": articles_container,
        "must_know_blocks": must_know_blocks,
    })


def filter_by_category(request, item_type_slug):
    current_category = None

    category_id = request.GET.get("category")
    if category_id is not None:
        try:
            current_category = Category.objects.get(id=category_id)
        except Category.DoesNotExist:
            raise Http404

    item_model = get_item_model(item_type_slug)
    if item_model is None:
        raise Http404

    ctype = ContentType.objects.get_for_model(item_model)

    # get itemtype
    try:
        item_type = ItemType.objects.get(ctype_id=ctype.id)
    except ItemType.DoesNotExist:
        raise Http404

    try:
        on_page = int(request.GET.get("op")) or ON_PAGE_DEFAULT
    except (TypeError, ValueError):
        on_page = ON_PAGE_DEFAULT

    if on_page not in ON_PAGE_VARIANTS:
        on_page = ON_PAGE_DEFAULT

    try:
        page = int(request.GET.get("p") or PAGE_DEFAULT)
    except (TypeError, ValueError):
        page = PAGE_DEFAULT

    order = None
    order_key = request.GET.get("o")
    if order_key is not None:
        order = ORDER_KEYS.get(order_key)

    if order is not None:
        items = item_model.objects.all(*order)
    else:
        items = item_model.objects.all()

    all_item_id_list = items.values_list("id", flat=True)

    #  get all categories with `current-item-model` goods
    categories = Category.objects.all()
    category_related_name, item_field = Category.qs_by_item_type(item_type_slug)
    if (category_related_name is not None) and (item_field is not None):
        conditions = {
            "%s__in" % item_field: all_item_id_list,
        }
        categories = getattr(categories, category_related_name, Category.objects.all()).filter(**conditions)

    # regroup categories in structure:
    # {
    #   category: [subcategory1, subcategory2],
    #   category: [subcategory1, subcategory2],
    # }
    structure = SortedDict()
    first_level_categories = categories.filter(level=0)
    second_level_categories = categories.filter(level=1)
    for first_level in first_level_categories:
        structure[first_level] = []

    for second_level in second_level_categories:
        if second_level.parent in structure.keys():
            structure[second_level.parent].append(second_level)
        else:
            structure[second_level.parent] = [second_level]

    if current_category is not None:
        items = items.filter(category=current_category)

    paginator = Paginator(items, on_page)

    try:
        items = paginator.page(page)
    except PageNotAnInteger:
        items = paginator.page(1)
    except EmptyPage:
        items = paginator.page(paginator.num_pages)

    items = paginator_befor_after(items, on_side=5)

    return render(request, "catalog/category.html", {
        "goods": items,
        "on_page_variants": ON_PAGE_VARIANTS,
        "on_page": on_page,
        "page": page,
        "item_type": item_type,
        "item_model": item_model,
        "order_variants": ORDER_KEYS,
        "structure": structure,
        "current_category": current_category,
    })


def choice_texture(request, item_type_slug, item_id):

    item_type_slug = Fresco.TYPE
    item_model = get_item_model(item_type_slug)
    if item_model is None:
        raise Http404

    ctype = ContentType.objects.get_for_model(item_model)

    try:
        item_type = ItemType.objects.get(ctype_id=ctype.id)
    except ItemType.DoesNotExist:
        raise Http404

    try:
        item = item_model.objects.get(id=item_id)
    except item_model.DoesNotExist:
        raise Http404

    return render(request, 'catalog/personalization/choice.html', {
        'item_model': item_model,
        'item_type': item_type,
        'item': item,
    })


def item_by_id(request, item_type_slug, item_id, texture_type=None):

    item_model = get_item_model(item_type_slug)
    if item_model is None:
        raise Http404

    ctype = ContentType.objects.get_for_model(item_model)

    try:
        item_type = ItemType.objects.get(ctype_id=ctype.id)
    except ItemType.DoesNotExist:
        raise Http404

    template = 'catalog/personalization/%s.html' % item_type_slug

    try:
        item = item_model.objects.get(id=item_id)
    except item_model.DoesNotExist:
        raise Http404

    textures = get_texture_type_by_item_model(item_model, texture_type)

    return render(request, template, {
        'item_model': item_model,
        'item_type': item_type,
        'item': item,
        'textures': textures,
    })


class SaleView(PaginationMixin, ListView):
    paginate_by = ON_PAGE_DEFAULT

    def get_paginate_by(self, *args, **kwargs):
        paginate = int(self.request.GET.get('op', self.paginate_by))
        if paginate in ON_PAGE_VARIANTS:
            return paginate
        return self.paginate_by

    def get_queryset(self):
        content_types = ItemType.objects.filter(enable=True)

        items = []
        for ctype in content_types:
            model = ctype.ctype.model_class()
            items += list(model.objects.all())
        return list(QuerySetJoin(items))


class SearchVew(PaginationMixin, ListView):
    template_name = 'catalog/search.html'
    paginate_by = ON_PAGE_DEFAULT

    def get_paginate_by(self, *args, **kwargs):
        paginate = int(self.request.GET.get('op', self.paginate_by))
        if paginate in ON_PAGE_VARIANTS:
            return paginate
        return self.paginate_by

    def get_queryset(self):
        content_types = ItemType.objects.filter(enable=True)

        if self.request.GET.get('category'):
            content_types = content_types.filter(pk=self.request.GET.get('category'))

        order = 'pk'
        if self.request.GET.get('goods_sort'):
            order = ORDER_KEYS.get(self.request.GET.get('goods_sort'))['order'][0]

        items = []
        for ctype in content_types:
            model = ctype.ctype.model_class()
            items += list(model.objects.order_by(order))
        return list(QuerySetJoin(items))

    def get_context_data(self, **kwargs):
        ctx = super(SearchVew, self).get_context_data(**kwargs)
        ctx['q'] = self.request.GET.get('q')
        ctx['order_variants'] = ORDER_KEYS
        ctx['on_page_variants'] = ON_PAGE_VARIANTS
        ctx['on_page'] = self.get_paginate_by()

        content_types = ItemType.objects.filter(enable=True)
        items = {}
        for ctype in content_types:
            model = ctype.ctype.model_class()
            items[ctype] = model.objects.count()
        ctx['catalog_items'] = items

        return ctx
