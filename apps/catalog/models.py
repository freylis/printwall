# coding: utf-8

from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from django.core.exceptions import ValidationError

from tinymce.models import HTMLField
from mptt.models import MPTTModel, TreeForeignKey
from sorl.thumbnail import ImageField

from printwall.utils import random_path
from printwall.models import BaseMoveModel
from apps.options.models import Texture


class ItemType(BaseMoveModel):

    class Meta:
        verbose_name = u"Тип изделия"
        verbose_name_plural = u"Типы изделий"

    title = models.CharField(u"Название", null=True, blank=False, max_length=255)
    main_title = models.CharField(u"Название для главной страницы", null=True, blank=False, max_length=255)
    ctype = models.ForeignKey("contenttypes.ContentType", null=True, blank=False,
                              verbose_name=u"Тип содержимого", unique=True)
    image = ImageField(u"Изображение на главной", null=True, blank=False, upload_to=random_path)
    description = HTMLField(u"Описание", null=True, blank=True)
    max_width = models.PositiveIntegerField(u'Максимальная ширина', null=True, blank=False)
    max_height = models.PositiveIntegerField(u'Максимальная высота', null=True, blank=False)
    enable = models.BooleanField(u"Отображать", blank=True, default=True)

    def __unicode__(self):
        return self.title

    def get_title(self):
        return self.title

    def get_url(self):
        return reverse("catalog_by_slug", args=[self.ctype.model_class().TYPE])

    def get_filter_url(self):
        return reverse('apps.catalog.views.filter_by_category', args=[self.ctype.model_class().TYPE])


class BaseImage(models.Model):

    class Meta:
        abstract = True

    image = ImageField(u"Изображение", null=True, blank=False, upload_to=random_path)
    title = models.CharField(u"Название", null=True, blank=False, max_length=255)
    item_type = None

    def __unicode__(self):
        return self.title


class BaseItem(BaseMoveModel):

    TYPE = None
    IMAGE_MODEL = None
    TEXTURE_TYPE = None

    HORIZONTAL = "horizontal"
    VERTICAL = "vertical"
    SQUARE = "square"

    VIEWS = (
        (HORIZONTAL, u"Горизонтальные"),
        (SQUARE, u"Квадратные"),
        (VERTICAL, u"Вертикальные"),
    )

    class Meta:
        abstract = True

    title = models.CharField(u"Название", null=True, blank=False, max_length=255)
    image = ImageField(u"Изображение", upload_to=random_path)
    price = models.DecimalField(u"Цена", null=True, blank=False, max_digits=7,
                                decimal_places=2)
    popular = models.BooleanField(u"Популярное", blank=True, default=False)
    exist = models.BooleanField(u"В наличии", blank=True, default=True)
    sale = models.BooleanField(u'Распродажа', default=False)
    discount = models.BooleanField(u"Скидка", blank=True, default=True)
    view = models.CharField(u"Расположение", null=True, blank=True, choices=VIEWS, max_length=20)
    creating = models.CharField(u"Изготовление", null=True, blank=False, max_length=50,
                                help_text=u"изготовление 2-3 дня", default=u"изготовление 2-3 дня")
    at_main = models.BooleanField(u"Отображать на главной", blank=True, default=False)
    created_date = models.DateTimeField(u"Дата создания", auto_now_add=True)
    updated_date = models.DateTimeField(u"Дата обновления", auto_now=True)
    category = models.ManyToManyField("catalog.Category", null=True, blank=True, verbose_name=u"Категории")

    def __unicode__(self):
        return self.title

    def get_title(self):
        return self.title

    def get_url(self):
        return reverse("item_by_id", args=[self.TYPE, self.id])

    def get_absolute_url(self):
        return self.get_url()

    def get_article(self):
        if self.sale:
            return "ID%sS%s" % (str(self.id).rjust(7, "0"), self.pk)
        return "ID%s" % str(self.id).rjust(7, "0")

    def get_price(self):
        return self.price or ""

    def get_main_title(self):
        content_type = ContentType.objects.get_for_model(self.__class__)
        try:
            item_type_instance = ItemType.objects.get(ctype_id=content_type.id)
        except ItemType.DoesNotExist:
            return self.get_title()
        return item_type_instance.main_title

    def get_image_model(self):
        return self.IMAGE_MODEL

    @classmethod
    def get_content_type(cls):
        return ContentType.objects.get_for_model(cls)


class FrescoImage(BaseImage):

    class Meta:
        verbose_name = u"Изображение фресок"
        verbose_name_plural = u"Изображения фресок"

    item = models.ForeignKey("catalog.Fresco")


class Fresco(BaseItem):

    TYPE = "fresco"
    IMAGE_MODEL = FrescoImage

    class Meta:
        verbose_name = u"Фреска"
        verbose_name_plural = u"Фрески"

    def get_url(self):
        return reverse("choice_texture", args=[self.__class__.TYPE, self.id])


class WallpaperImage(BaseImage):

    class Meta:
        verbose_name = u"Изображение фотообоев"
        verbose_name_plural = u"Изображения фотообоев"

    item = models.ForeignKey("catalog.Wallpaper")


class Wallpaper(BaseItem):

    TYPE = "wallpaper"
    IMAGE_MODEL = WallpaperImage
    TEXTURE_TYPE = Texture.WALLPAPER

    class Meta:
        verbose_name = u"Фотообои"
        verbose_name_plural = u"Фотообои"


class MosaicImage(BaseImage):

    class Meta:
        verbose_name = u"Изображение фрески-мозайки"
        verbose_name_plural = u"Изображения фресок-мозаек"

    item = models.ForeignKey("catalog.Mosaic")


class Mosaic(BaseItem):

    TYPE = "mosaic"
    IMAGE_MODEL = MosaicImage
    TEXTURE_TYPE = Texture.MOSAIC

    class Meta:
        verbose_name = u"Фреска-мозайка"
        verbose_name_plural = u"Фрески-мозайки"


class PictureImage(BaseImage):

    class Meta:
        verbose_name = u"Изображение картины"
        verbose_name_plural = u"Изображения картин"

    item = models.ForeignKey("catalog.Picture")


class Picture(BaseItem):

    TYPE = "picture"
    IMAGE_MODEL = PictureImage
    TEXTURE_TYPE = Texture.PICTURE

    class Meta:
        verbose_name = u"Картина"
        verbose_name_plural = u"Картины"


class PosterImage(BaseImage):

    class Meta:
        verbose_name = u"Изображение постера"
        verbose_name_plural = u"Изображения постеров"

    item = models.ForeignKey("catalog.Poster")


class Poster(BaseItem):

    TYPE = "poster"
    IMAGE_MODEL = PosterImage
    TEXTURE_TYPE = Texture.POSTER

    class Meta:
        verbose_name = u"Постер"
        verbose_name_plural = u"Постеры"


class Category(MPTTModel):

    RELATED_ITEM_MODELS = {
        Fresco.TYPE: ("fresco_set", "fresco"),
        Wallpaper.TYPE: ("wallpaper_set", "wallpaper"),
        Mosaic.TYPE: ("mosaic_set", "mosaic"),
        Picture.TYPE: ("picture_set", "picture"),
        Poster.TYPE: ("poster_set", "poster"),
    }

    class Meta:
        verbose_name = u"Категория"
        verbose_name_plural = u"Категории"

    title = models.CharField(u"Название", null=True, blank=False, max_length=255)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children',
                            verbose_name=u"Родитель")
    created_date = models.DateTimeField(u"Дата создания", auto_now_add=True)
    updated_date = models.DateTimeField(u"Дата обновления", auto_now=True)

    def __unicode__(self):
        return self.title

    def get_title(self):
        return self.title

    def get_url(self, item_type):
        """
        get url of category by item type slug
        :param item_type: slug for model of item
        :return: url of category or empty string
        """
        item_model = get_item_model(item_type)
        if item_model is not None:
            return "/category-url"
        return ""

    def clean(self):

        # level must be 0 or 1
        if self.parent and (self.parent.level > 0):
            raise ValidationError(u"Допустим только один уровень вложенности")

    @classmethod
    def qs_by_item_type(cls, item_type):
        return cls.RELATED_ITEM_MODELS.get(item_type) or (None, None)


ITEM_MODELS = {
    "fresco": Fresco,
    "wallpaper": Wallpaper,
    "mosaic": Mosaic,
    "picture": Picture,
    "poster": Poster,
}


def get_item_model(item_type):
    return ITEM_MODELS.get(item_type)
