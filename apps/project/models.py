# coding: utf-8

from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey

from jsonfield import JSONField

from printwall.utils import get_random_string


class Project(models.Model):

    class Meta:
        verbose_name = u'Проект'
        verbose_name_plural = u'Проекты'

    STRETCHER_BLACK = 'stretcher_black'
    STRETCHER_WHITE = 'stretcher_white'
    STRETCHER_PICTURE = 'stretcher_picture'
    STRETCHER_ROLL = 'stretcher_roll'
    STRETCHERS = (
        (STRETCHER_BLACK, u'Черный подрамник'),
        (STRETCHER_WHITE, u'Белый подрамник'),
        (STRETCHER_PICTURE, u'Белый с рисунком'),
        (STRETCHER_ROLL, u'Рулон'),
    )

    COVER_BAGUETTE = 'baguette'
    COVER_GEL = 'gel'
    COVER_LACQUER = 'lacquer'
    COVERS = (
        (COVER_BAGUETTE, u'черный Багет'),
        (COVER_GEL, u'белый Фактурный гель'),
        (COVER_LACQUER, u'рисунок Лак'),
    )

    pid = models.CharField(u'Идентификатор проекта', null=True, blank=True, max_length=16, editable=False)
    content_type = models.ForeignKey("contenttypes.ContentType", null=True, blank=False,
                                     verbose_name=u"Тип содержимого")
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    data = JSONField(u'Данные заказ', null=True, blank=False)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.pid

    def save(self, *args, **kwargs):
        if self.id is None:
            self.pid = get_random_string(16)
        super(Project, self).save(*args, **kwargs)