# coding: utf-8

from django.contrib import admin

from apps.project.models import Project


class ProjectAdmin(admin.ModelAdmin):
    pass
admin.site.register(Project, ProjectAdmin)