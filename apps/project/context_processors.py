# coding: utf-8

from apps.project.models import Project


def project_constants(request):
    return {
        'STRETCHER_BLACK': Project.STRETCHER_BLACK,
        'STRETCHER_WHITE': Project.STRETCHER_WHITE,
        'STRETCHER_PICTURE': Project.STRETCHER_PICTURE,
        'STRETCHER_ROLL': Project.STRETCHER_ROLL,
        'COVER_BAGUETTE': Project.COVER_BAGUETTE,
        'COVER_LACQUER': Project.COVER_LACQUER,
        'COVER_GEL': Project.COVER_GEL,
    }

